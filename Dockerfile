FROM docker:latest
RUN apk add --update --no-cache gcc build-base libffi-dev libressl-dev bash git gettext curl python3 python3-dev py3-pip \
    && pip install --no-cache-dir --upgrade docker-compose